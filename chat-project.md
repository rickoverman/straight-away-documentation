
# Chat project

This document describes the challenges when creating javascript Chat functionalities in our `Straight away Home` project.

Also it would be wise to note that to make the frontend javascript more easy to manage we need to change the way we write/compile our javascript a little. 

The current way the frontend javascript is implemented is not the best, fastest, easiest and most scalable way. The best way is to make use of the already preferred [VanillaJS](https://stackoverflow.com/questions/20435653/what-is-vanillajs) together with the [Node.js module system](https://nodejs.org/api/modules.html) architecture. More on info on this at the chapter [General Javascript Best Practices](#general-javascript-best-practices).

## Chat Frontend

In case of the Chat component it would logical to make use of a [progressive framework](https://www.interactivated.me/blog/progressive-javascript-framework/). In this case we use [vue.js](https://vuejs.org/) because it's modular and has a small footprint (it's only [33.30KB]( [https://vuejs.org/v2/guide/installation.html](https://vuejs.org/v2/guide/installation.html)) and smaller and faster than [React.js](https://reactjs.org/)).

The use [vue.js](https://vuejs.org/) greatly decreases the lines of code needed for the more complex javascript components, thus greatly increases the development speed. Also it can be easily used next to our current code base, will make the javascript code more manageable and less sensitive to bugs and make it more secure than using only [VanillaJS](https://stackoverflow.com/questions/20435653/what-is-vanillajs).

### Used frontend libraries

- [vue.js](https://vuejs.org/)
- [socket.io](https://socket.io/)

Please look at the [General Javascript Best Practices](#general-javascript-best-practices) chapter for more comments on writing easier and scalable frontend javascript code.

## Chat Backend

Requires a socket server, best and most scalable option would be a [node express](https://expressjs.com/) server.

### Used backend libraries

- [express.js](https://expressjs.com/)
- [socket.io](https://socket.io/)
- [mysql](https://www.npmjs.com/package/mysql)

## Socket.io

We need a socket server to manage the chat functionalities. The best and easiest way to set up a socket server is through a node/express/socket.io setup. The [socket.io](https://socket.io/) is a very mature framework that can handle all of our requirements when creating a client and coach coaching platform.

> Internet Explorer 9 does not have support for WebSockets, it will degrade the connection to xhr-polling. By default, socket.io is configured to use the following transports (in this order): websocket, htmlfile, xhr-polling and jsonp-polling

It is most scalable and it would be easy to implement [sending of photo's](https://stackoverflow.com/questions/26331787/socket-io-node-js-simple-example-to-send-image-files-from-server-to-client) through the chat and even include [streaming video/WebRTC](https://gabrieltanner.org/blog/webrtc-video-broadcast) at a later stage of the project.

## Azure

It is possible to setup a socket server with azure. This is done through [Azure Web and Worker Roles](https://dzone.com/articles/running-socketio-windows-azure). This makes also use of the node/express/socket.io setup architecture.

So the socket server code required by an [Azure Web Role](https://docs.microsoft.com/nl-nl/azure/cloud-services/cloud-services-choose-me) would be (almost)exactly the same as a VM.

## General Javascript Best Practices

Because the project would require more and more javascript as it progresses. It would be wise to make use of [Node.js Modules](https://nodejs.org/api/modules.html) in the frontend of the project, this will make the project much more easy to manage and it would be easy to include extra functionalities through the node package manager. This would be done through [Webpack](https://webpack.js.org/) which can be easily [implemented](https://www.npmjs.com/package/gulp-webpack) in our current Gulp development workflow.
